<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>App Food</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/materialize.min.css" />">
    <link rel="stylesheet" href="<c:url value="/resources/css/materialdesignicons.min.css"  />">
    <link rel="stylesheet" href="<c:url value="/resources/css/materialize.css"  />">
    <link rel="stylesheet" href="<c:url value="/resources/css/global.css"  />">
    <script type="text/javascript" src="<c:url value="/resources/js/jquery-2.1.1.min.js"  />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/materialize.js"  />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/main.js"  />"></script>
    <style>
        @media only screen and (min-width: 992px) {
            html {
                font-size: 11px !important;
            }
        }
    </style>
</head>
<body>

<nav>
    <div class="nav-wrapper">
        &nbsp; &nbsp; &nbsp; <a href="/index" class="brand-logo">App Food</a>
        <ul class="right hide-on-med-and-down">
            <!-- Dropdown Trigger -->
            <li><a href="#">Ajouter </a></li>
            <li><a href="/addMenuItem">Menu Item</a></li>
            <li><a href="/addRestaurant">Restaurant</a></li>
            <li><a href="#">Manage </a></li>
            <li><a href="/displayOrders">Orders</a></li>
            <li><a href="/displayClients">Clients</a></li>
        </ul>
    </div>
</nav>


<div class="row container">
    <div>
        <p></p>
    </div>

    <div class="m8 offset-2">
        Today, they serve ${menuItems.size()} delicious meals! Have a look, pick any quantity and submit.
        <form action="order" method="POST">

            <c:forEach var="menuItems" items="${menuItems}">
                <div class="col s12 m3 l3">
                    <div class="card sticky-action" style="overflow: hidden;">

                        <div class="card-title">
                            <div class="small pl-10">
                                    ${menuItems.category.name}
                            </div>
                        </div>

                        <div class="card-image waves-effect waves-block waves-light">
                            <img class="activator"
                                 src="/resources/img/${menuItems.category.name}.jpg">
                        </div>
                        <div class="card-content">
                    <span class="card-title grey-text text-darken-4"><h4>${menuItems.label} </h4>
                       </span>

                            <p class="justify-align"><br/>
                                    ${menuItems.description} <br/>
                                Price: ${menuItems.price} &euro;<br/>

                            </p>
                            Quantity: <input type="text" name="${menuItems.id}" value="" class="smallInput" />
                        </div>





                    </div>
                </div>
            </c:forEach>
            <div class="center-align"><input type="submit" class="btn" value="ORDER"></div>
        </form>

    </div>
</div>
</body>
</html>