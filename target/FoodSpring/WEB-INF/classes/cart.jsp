<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>App Food</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/materialize.min.css" />">
    <link rel="stylesheet" href="<c:url value="/resources/css/materialdesignicons.min.css"  />">
    <link rel="stylesheet" href="<c:url value="/resources/css/materialize.css"  />">
    <link rel="stylesheet" href="<c:url value="/resources/css/global.css"  />">
    <script type="text/javascript" src="<c:url value="/resources/js/jquery-2.1.1.min.js"  />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/materialize.js"  />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/main.js"  />"></script>
    <style>
        @media only screen and (min-width: 992px) {
            html {
                font-size: 11px !important;
            }
        }
    </style>
</head>
<body>

<nav>
    <div class="nav-wrapper">
        &nbsp; &nbsp; &nbsp; <a href="/index" class="brand-logo">App Food</a>
        <ul class="right hide-on-med-and-down">
            <!-- Dropdown Trigger -->
            <li><a href="#">Ajouter </a></li>
            <li><a href="/addMenuItem">Menu Item</a></li>
            <li><a href="/addRestaurant">Restaurant</a></li>
            <li><a href="#">Manage </a></li>
            <li><a href="/displayOrders">Orders</a></li>
            <li><a href="/displayClients">Clients</a></li>
        </ul>
    </div>
</nav>
<div class="row container">
    <div>
        <p></p>
    </div>

    <div class="m8 offset-2">


        You choose ${itemsOrdered.size()} products.<br/>
        <c:forEach var="itemsOrdered" items="${itemsOrdered}">
        <p>
            Category: ${itemsOrdered} <br/>
        </p>
        </c:forEach>
    </div>
</div>

</body>
</html>