package fr.humanbooster.fx.food.business;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Human Booster on 07/03/2017.
 */
@Entity
public class Restaurant {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    private String nom;
    private String description;
    private String address;


    // la liste des commandes qui appartiennent au restaurant
    @OneToMany(mappedBy = "restaurant")
    private List<Commande> commandes;

    // la liste des éléments de menu qui appartiennent au restaurant
    @OneToMany(mappedBy="restaurant", fetch = FetchType.EAGER)
    private List<MenuItem> items;

    // menuItem à laquelle la commande line appartient
    // @ManyToOne
//    private MenuItem menuItem;

    public Restaurant(){}

    public Restaurant(String nom, String description, String address, List<MenuItem> items) {
        this.nom = nom;
        this.description = description;
        this.address = address;
        this.items = items;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<MenuItem> getItems() {
        return items;
    }

    public void setItems(List<MenuItem> items) {
        this.items = items;
    }
}
