package fr.humanbooster.fx.food.business;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Human Booster on 07/03/2017.
 */
@Entity
public class Client {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    private String name;
    private String adress;
    private String cbClient;

    // les commandes d'un client
    @OneToMany(mappedBy = "client")
    private List<Commande> commandesClient;

    public Client(){}

    public Client(String name, String adress, String cbClient) {
        this.name = name;
        this.adress = adress;
        this.cbClient = cbClient;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getCbClient() {
        return cbClient;
    }

    public void setCbClient(String cbClient) {
        this.cbClient = cbClient;
    }


}
