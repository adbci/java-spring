package fr.humanbooster.fx.food.business;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by Human Booster on 07/03/2017.
 */
@Entity
public class MenuItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String label;
    private String description;
    private Float price;

    // category à laquelle l'item appartient
    @ManyToOne
    private Category category;

    // restaurant auquel l'item appartient
    @ManyToOne
    private Restaurant restaurant;

    public MenuItem(String label, String description, Float price, Category category, Restaurant restaurant, List<CommandLine> commandLines) {
        this.label = label;
        this.description = description;
        this.price = price;
        this.category = category;
        this.restaurant = restaurant;
        this.commandLines = commandLines;
    }

    // ligne de commande à laquelle l'itam appartient
    @OneToMany(mappedBy = "menuItem")
    private List<CommandLine> commandLines;


    public MenuItem() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public void setCommandLines(List<CommandLine> commandLines) {
        this.commandLines = commandLines;
    }
}
