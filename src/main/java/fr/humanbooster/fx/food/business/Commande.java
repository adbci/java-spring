package fr.humanbooster.fx.food.business;

import javax.persistence.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Human Booster on 07/03/2017.
 */
@Entity
public class Commande {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    // le restaurant à laquelle la commande appartient
    @ManyToOne
    private Restaurant restaurant;

    // client a laquelle la commande appartient
    @ManyToOne
    private Client client;

    // les lignes de la commande
    // preciser mappedby
    @OneToMany(mappedBy = "commande")
    private List<CommandLine> commandLines;

    public Commande() {}

    public Commande(Restaurant restaurant, Client client) {
        this.restaurant = restaurant;
        this.client = client;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
