package fr.humanbooster.fx.food.business;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Human Booster on 07/03/2017.
 */
@Entity
public class CommandLine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int quantity;

    // commande a laquelle la line command appartient
    @ManyToOne
    private Commande commande;

    // client a laquelle la commande line appartient
    @ManyToOne
    private Client client;

    // menuItem à laquelle la commande line appartient
    @ManyToOne
    private MenuItem menuItem;

    public CommandLine(){}

    public CommandLine(int quantity) {
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public MenuItem getMenuItem() {
        return menuItem;
    }

    public void setMenuItem(MenuItem menuItem) {
        this.menuItem = menuItem;
    }
}
