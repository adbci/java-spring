package fr.humanbooster.fx.food.business;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Human Booster on 07/03/2017.
 */
@Entity
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;

    // les items de la category
    // toujours preciser mappedby
    @OneToMany(mappedBy="category")
    private List<MenuItem> menuItems;

    public Category() {}

    public Category(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
