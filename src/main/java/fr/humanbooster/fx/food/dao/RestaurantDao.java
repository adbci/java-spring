package fr.humanbooster.fx.food.dao;

import fr.humanbooster.fx.food.business.MenuItem;
import fr.humanbooster.fx.food.business.Restaurant;

import java.util.List;

/**
 * Created by Human Booster on 07/03/2017.
 */
public interface RestaurantDao {
    public List<Restaurant> findAll();

    public Restaurant create(Restaurant restaurant);

    public Restaurant update(Restaurant restaurant);

    public boolean delete(int id);

    public Restaurant findById(int id);

}