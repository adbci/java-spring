package fr.humanbooster.fx.food.dao.impl;

import fr.humanbooster.fx.food.business.Category;
import fr.humanbooster.fx.food.business.Restaurant;
import fr.humanbooster.fx.food.dao.CategoryDao;
import fr.humanbooster.fx.food.dao.RestaurantDao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Human Booster on 07/03/2017.
 */
@Repository
public class CategoryDaoImpl implements CategoryDao {
    @Autowired
    private SessionFactory sf;

    public List<Category> findAll() {

        return sf.getCurrentSession().createQuery("FROM Category ").getResultList();

    }

    public Category create(Category category) {

        sf.getCurrentSession().save(category);
        return category;

    }

    public Category update(Category category) {
        sf.getCurrentSession().update(category);
        return category;
    }

    public boolean delete(int id) {
        Category category = findById(id);
        if (category == null) {
            return false;
        }
        sf.getCurrentSession().delete(category);
        return true;
    }

    public Category findById(int id) {
        return sf.getCurrentSession().byId(Category.class).load(id);
    }

}
