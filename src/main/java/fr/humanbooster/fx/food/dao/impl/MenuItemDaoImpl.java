package fr.humanbooster.fx.food.dao.impl;

import fr.humanbooster.fx.food.business.Client;
import fr.humanbooster.fx.food.business.MenuItem;
import fr.humanbooster.fx.food.dao.ClientDao;
import fr.humanbooster.fx.food.dao.MenuItemDao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Human Booster on 07/03/2017.
 */
@Repository
public class MenuItemDaoImpl implements MenuItemDao {
    @Autowired
    private SessionFactory sf;

    public List<MenuItem> findAllByIdRestaurant(int idRestaurant) {
        System.out.print("find menu by restaurant id");
        return sf.getCurrentSession().createQuery("FROM MenuItem WHERE restaurant.id=:id").setParameter("id",idRestaurant).getResultList();
    }

    public MenuItem create(MenuItem menuItem) {

        sf.getCurrentSession().save(menuItem);
        return menuItem;

    }

    public MenuItem update(MenuItem menuItem) {
        sf.getCurrentSession().update(menuItem);
        return menuItem;
    }

    public boolean delete(int id) {
        MenuItem menuItem = findById(id);
        if (menuItem == null) {
            return false;
        }
        sf.getCurrentSession().delete(menuItem);
        return true;
    }

    public MenuItem findById(int id) {
        return sf.getCurrentSession().byId(MenuItem.class).load(id);
    }

}
