package fr.humanbooster.fx.food.dao.impl;

import fr.humanbooster.fx.food.business.Client;
import fr.humanbooster.fx.food.dao.ClientDao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Human Booster on 07/03/2017.
 */
@Repository
public class ClientDaoImpl implements ClientDao {
    @Autowired
    private SessionFactory sf;

    public List<Client> findAll() {
        return sf.getCurrentSession().createQuery("FROM Client").getResultList();
    }

    public Client create(Client client) {

        sf.getCurrentSession().save(client);
        return client;

    }

    public Client update(Client client) {
        sf.getCurrentSession().update(client);
        return client;
    }

    public boolean delete(int id) {
        Client client = findById(id);
        if (client == null) {
            return false;
        }
        sf.getCurrentSession().delete(client);
        return true;
    }

    public Client findById(int id) {
        return sf.getCurrentSession().byId(Client.class).load(id);
    }

}
