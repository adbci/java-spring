package fr.humanbooster.fx.food.dao;

import fr.humanbooster.fx.food.business.CommandLine;
import fr.humanbooster.fx.food.business.MenuItem;

import java.util.List;

/**
 * Created by Human Booster on 07/03/2017.
 */
public interface CommandLineDao {
//    public List<CommandLine> findAllByIdCommand(int idCommand);

    public CommandLine create(CommandLine commandLine);


}