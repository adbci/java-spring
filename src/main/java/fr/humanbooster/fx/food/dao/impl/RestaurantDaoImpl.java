package fr.humanbooster.fx.food.dao.impl;

import fr.humanbooster.fx.food.business.MenuItem;
import fr.humanbooster.fx.food.business.Restaurant;
import fr.humanbooster.fx.food.dao.MenuItemDao;
import fr.humanbooster.fx.food.dao.RestaurantDao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Human Booster on 07/03/2017.
 */
@Repository
public class RestaurantDaoImpl implements RestaurantDao {
    @Autowired
    private SessionFactory sf;

    public List<Restaurant> findAll() {

        return sf.getCurrentSession().createQuery("FROM Restaurant ").getResultList();

    }

    public Restaurant create(Restaurant restaurant) {

        sf.getCurrentSession().save(restaurant);
        return restaurant;

    }

    public Restaurant update(Restaurant restaurant) {
        sf.getCurrentSession().update(restaurant);
        return restaurant;
    }

    public boolean delete(int id) {
        Restaurant restaurant = findById(id);
        if (restaurant == null) {
            return false;
        }
        sf.getCurrentSession().delete(restaurant);
        return true;
    }

    public Restaurant findById(int id) {
        return sf.getCurrentSession().byId(Restaurant.class).load(id);
    }

}
