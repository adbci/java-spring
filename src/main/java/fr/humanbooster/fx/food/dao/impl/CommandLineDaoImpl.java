package fr.humanbooster.fx.food.dao.impl;

import fr.humanbooster.fx.food.business.CommandLine;
import fr.humanbooster.fx.food.business.MenuItem;
import fr.humanbooster.fx.food.dao.CommandLineDao;
import fr.humanbooster.fx.food.dao.MenuItemDao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Human Booster on 07/03/2017.
 */
@Repository
public class CommandLineDaoImpl implements CommandLineDao {
    @Autowired
    private SessionFactory sf;
/*
    public List<CommandLine> findAllByIdCommand(int idCommand) {
        System.out.print("find command lines by command id");
        return sf.getCurrentSession().createQuery("FROM CommandLine WHERE commande.id=:id").setParameter("id",idCommand).getResultList();
    }
*/
    public CommandLine create(CommandLine commandLine) {
        sf.getCurrentSession().save(commandLine);
        return commandLine;
    }


}
