package fr.humanbooster.fx.food.dao;

import fr.humanbooster.fx.food.business.Category;
import fr.humanbooster.fx.food.business.Restaurant;

import java.util.List;

/**
 * Created by Human Booster on 07/03/2017.
 */
public interface CategoryDao {
    public List<Category> findAll();

    public Category create(Category category);

    public Category findById(int idCategory);

    public Category update(Category category);

    public boolean delete(int id);

}