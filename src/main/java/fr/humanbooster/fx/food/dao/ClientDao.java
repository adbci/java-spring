package fr.humanbooster.fx.food.dao;

import fr.humanbooster.fx.food.business.Client;

import java.util.Date;
import java.util.List;

/**
 * Created by Human Booster on 07/03/2017.
 */
public interface ClientDao {
    public List<Client> findAll();

    public Client create(Client client);

    public Client update(Client client);

    public boolean delete(int id);

}
