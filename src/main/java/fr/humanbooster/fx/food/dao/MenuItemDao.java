package fr.humanbooster.fx.food.dao;

import fr.humanbooster.fx.food.business.Client;
import fr.humanbooster.fx.food.business.MenuItem;

import java.util.List;

/**
 * Created by Human Booster on 07/03/2017.
 */
public interface MenuItemDao {
    public List<MenuItem> findAllByIdRestaurant(int idRestaurant);

    public MenuItem create(MenuItem menuItem);

    public MenuItem update(MenuItem menuItem);

    public boolean delete(int id);

}