package fr.humanbooster.fx.food.service;

import fr.humanbooster.fx.food.business.MenuItem;
import fr.humanbooster.fx.food.business.Restaurant;

import java.util.List;


public interface RestaurantService {

    public List<Restaurant> getAllRestaurants();

    public boolean addRestaurant(Restaurant restaurant);

    public Restaurant getRestaurantById(int idRestaurant);

}
