package fr.humanbooster.fx.food.service;

import fr.humanbooster.fx.food.business.Client;
import fr.humanbooster.fx.food.dao.ClientDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface ClientService {

    public List<Client> getAllClients();


}
