package fr.humanbooster.fx.food.service.impl;

import fr.humanbooster.fx.food.business.Client;
import fr.humanbooster.fx.food.business.MenuItem;
import fr.humanbooster.fx.food.dao.ClientDao;
import fr.humanbooster.fx.food.dao.MenuItemDao;
import fr.humanbooster.fx.food.service.MenuItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional // pour que Spring se charge des transactions au niveau des services
public class MenuItemServiceImpl implements MenuItemService {

    @Autowired
    // private EnqueteDao ed = new EnqueteDaoImpl();
    // on enleve tous les new = ...
    private MenuItemDao mid;

    @Transactional(readOnly = true)
    public List<MenuItem> getAllMenuItemsById(int idRestaurant) {
        return mid.findAllByIdRestaurant(idRestaurant);
    }

    public boolean addMenuItem(MenuItem menuItem) {
        menuItem = mid.create(menuItem);
        if (menuItem != null) {
            return true;
        }
        return false;
    }

}
