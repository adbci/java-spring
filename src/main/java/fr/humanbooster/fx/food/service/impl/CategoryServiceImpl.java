package fr.humanbooster.fx.food.service.impl;

import fr.humanbooster.fx.food.business.Category;
import fr.humanbooster.fx.food.business.Restaurant;
import fr.humanbooster.fx.food.dao.CategoryDao;
import fr.humanbooster.fx.food.dao.RestaurantDao;
import fr.humanbooster.fx.food.service.CategoryService;
import fr.humanbooster.fx.food.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional // pour que Spring se charge des transactions au niveau des services
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    // private EnqueteDao ed = new EnqueteDaoImpl();
    // on enleve tous les new = ...
    private CategoryDao cd;

    @Transactional(readOnly = true)
    public List<Category> getAllCategories() {
        return cd.findAll();
    }

    public boolean addCategory(Category category) {
        category = cd.create(category);
        if (category != null) {
            return true;
        }
        return false;
    }

    public Category findCategoryById(int idCategory){
        Category category = cd.findById(idCategory);
        return category;
    }

}
