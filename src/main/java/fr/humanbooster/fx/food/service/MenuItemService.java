package fr.humanbooster.fx.food.service;

import fr.humanbooster.fx.food.business.Client;
import fr.humanbooster.fx.food.business.MenuItem;
import fr.humanbooster.fx.food.service.impl.MenuItemServiceImpl;

import java.util.List;


public interface MenuItemService  {

    public List<MenuItem> getAllMenuItemsById(int idRestaurant);

    public boolean addMenuItem(MenuItem menuItem);


}
