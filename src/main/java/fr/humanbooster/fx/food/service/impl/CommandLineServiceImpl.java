package fr.humanbooster.fx.food.service.impl;

import fr.humanbooster.fx.food.business.CommandLine;
import fr.humanbooster.fx.food.service.CommandLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional // pour que Spring se charge des transactions au niveau des services
public class CommandLineServiceImpl implements CommandLineService {

    @Autowired
    // on enleve tous les new = ...
    private CommandLineService cld;

   /* @Transactional(readOnly = true)
    public List<CommandLineDao> getAllMenuItemsById(int idCommand) {
        return cld.getAllCommandsLineByCommandId(idCommand);
    }
*/
    public boolean addCommandLine(CommandLine commandLine) {
        if (cld.addCommandLine(commandLine)) {
            return true;
        }
        return false;
    }

}
