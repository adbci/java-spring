package fr.humanbooster.fx.food.service.impl;

import fr.humanbooster.fx.food.business.MenuItem;
import fr.humanbooster.fx.food.business.Restaurant;
import fr.humanbooster.fx.food.dao.MenuItemDao;
import fr.humanbooster.fx.food.dao.RestaurantDao;
import fr.humanbooster.fx.food.service.MenuItemService;
import fr.humanbooster.fx.food.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional // pour que Spring se charge des transactions au niveau des services
public class RestaurantServiceImpl implements RestaurantService {

    @Autowired
    // private EnqueteDao ed = new EnqueteDaoImpl();
    // on enleve tous les new = ...
    private RestaurantDao rd;

    @Transactional(readOnly = true)
    public List<Restaurant> getAllRestaurants() {
        return rd.findAll();
    }


    @Transactional(readOnly = true)
    public Restaurant getRestaurantById(int idRestaurant) {
        return rd.findById(idRestaurant);
    }

    public boolean addRestaurant(Restaurant restaurant) {
        restaurant = rd.create(restaurant);
        if (restaurant != null) {
            return true;
        }
        return false;
    }

}
