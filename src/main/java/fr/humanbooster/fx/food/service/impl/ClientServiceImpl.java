package fr.humanbooster.fx.food.service.impl;

import fr.humanbooster.fx.food.business.Client;
import fr.humanbooster.fx.food.dao.ClientDao;
import fr.humanbooster.fx.food.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional // pour que Spring se charge des transactions au niveau des services
public class ClientServiceImpl implements ClientService {

    @Autowired
    // private EnqueteDao ed = new EnqueteDaoImpl();
    // on enleve tous les new = ...
    private ClientDao cd;

    @Transactional(readOnly = true)
    public List<Client> getAllClients() {
        return cd.findAll();
    }


}
