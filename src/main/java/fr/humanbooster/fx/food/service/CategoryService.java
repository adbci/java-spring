package fr.humanbooster.fx.food.service;

import fr.humanbooster.fx.food.business.Category;
import fr.humanbooster.fx.food.business.Restaurant;

import java.util.List;


public interface CategoryService {

    public List<Category> getAllCategories();

    public boolean addCategory(Category category);

    public Category findCategoryById(int idCategory);


}
