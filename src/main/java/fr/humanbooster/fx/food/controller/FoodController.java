package fr.humanbooster.fx.food.controller;

import fr.humanbooster.fx.food.business.Category;
import fr.humanbooster.fx.food.business.CommandLine;
import fr.humanbooster.fx.food.business.MenuItem;
import fr.humanbooster.fx.food.business.Restaurant;
import fr.humanbooster.fx.food.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

/**
 * Created by Human Booster on 06/03/2017.
 */
@Controller
// classe qui traite les requetes http
// en remplacement des servlets précédemments écrites
public class FoodController {

    // injection d'une dépendance au service EnqueteService
    @Autowired // on declare un objet en précisant le nom d'une interface
    private RestaurantService rs;

    @Autowired
    private MenuItemService mis;

    @Autowired
    private CommandLineService cls;

    @Autowired
    private CategoryService cs;

    @RequestMapping(value = {"index", "accueil", "/"}, method = RequestMethod.GET)
    // on définit la ou les URL prises en charge par la méthode accueil
    // cette méthode est invoquée uniquement face à une requete HTTP de type get

    public ModelAndView accueil() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("index");

        // recuperation des restaurants
        List<Restaurant> restaurants = rs.getAllRestaurants();

        // mise a dispo dans la JSP
        mav.getModel().put("restaurants", restaurants);

        return mav; // le return redirige vers la page index.jsp car un
        // viewresolver a été défini dans le fichier spring-servlet.xml
    }


    @RequestMapping(value = {"addRestaurant"}, method = RequestMethod.GET)
    // on définit la ou les URL prises en charge par la méthode accueil
    // cette méthode est invoquée uniquement face à une requete HTTP de type get

    public ModelAndView addRestaurant() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("addRestaurant");

        String message = "get";
        mav.getModel().put("message", message);
        return mav;
    }


    @RequestMapping(value = {"addRestaurant"}, method = RequestMethod.POST)
    // on définit la ou les URL prises en charge par la méthode accueil
    // cette méthode est invoquée uniquement face à une requete HTTP de type get

    public ModelAndView addRestaurantPOST(@RequestParam Map<String, Object> map) {

        ModelAndView mav = new ModelAndView();
        mav.setViewName("addRestaurant");

        Restaurant restaurant = new Restaurant();
        restaurant.setAddress(map.get("address").toString());
        restaurant.setDescription(map.get("description").toString());
        restaurant.setNom(map.get("nom").toString());

        String message;
        boolean error;
        if (rs.addRestaurant(restaurant)) {
            message = "Restaurant added successfully";
            error = false;
        } else {
            message = "Error while adding restaurant!";
            error = true;
        }
        mav.getModel().put("message", message);
        mav.getModel().put("error", error);
        return mav;
    }


    @RequestMapping(value = {"addMenuItem"}, method = RequestMethod.GET)
    // on définit la ou les URL prises en charge par la méthode accueil
    // cette méthode est invoquée uniquement face à une requete HTTP de type get

    public ModelAndView addMenuItem() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("addMenuItem");

        // recuperer la liste des restaurants
        List<Restaurant> restaurants = rs.getAllRestaurants();
        // recuperer la liste des categories
        List<Category> categories = cs.getAllCategories();

        mav.getModel().put("restaurants", restaurants);
        mav.getModel().put("categories", categories);

        String message = "get";
        mav.getModel().put("message", message);

        return mav;
    }

    @RequestMapping(value = {"addMenuItem"}, method = RequestMethod.POST)
    // on définit la ou les URL prises en charge par la méthode accueil
    // cette méthode est invoquée uniquement face à une requete HTTP de type get

    public ModelAndView addMenuItemPOST(@RequestParam Map<String, Object> map) {

        ModelAndView mav = new ModelAndView();
        mav.setViewName("addMenuItem");

        MenuItem menuItem = new MenuItem();
        menuItem.setDescription(map.get("description").toString());
        menuItem.setCategory(cs.findCategoryById(Integer.parseInt(map.get("choixCategory").toString())));
        menuItem.setRestaurant(rs.getRestaurantById(Integer.parseInt(map.get("choixRestaurant").toString())));
        menuItem.setLabel(map.get("label").toString());
        menuItem.setPrice(Float.parseFloat(map.get("price").toString()));

        String message;
        boolean error;
        if (mis.addMenuItem(menuItem)) {
            message = "Menu item added successfully";
            error = false;
        } else {
            message = "Error while adding menu item!";
            error = true;
        }
        mav.getModel().put("message", message);
        mav.getModel().put("error", error);

        return mav;
    }


    @RequestMapping(value = {"chooseRestaurant"}, method = RequestMethod.POST)
    // on définit la ou les URL prises en charge par la méthode accueil
    // cette méthode est invoquée uniquement face à une requete HTTP de type get

    public ModelAndView chooseRestaurant(@RequestParam String choixRestaurant) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("menu");

        // recuperation des items de chaque restaurant
        List<MenuItem> menuItems = mis.getAllMenuItemsById(Integer.parseInt(choixRestaurant.toString()));

        // mise a dispo dans la JSP
        mav.getModel().put("menuItems", menuItems);
        mav.getModel().put("idRestaurant", Integer.parseInt(choixRestaurant.toString()));

        return mav; // le return redirige vers la page index.jsp car un
        // viewresolver a été défini dans le fichier spring-servlet.xml
    }

    @RequestMapping(value = {"order"}, method = RequestMethod.POST)
    // on définit la ou les URL prises en charge par la méthode accueil
    // cette méthode est invoquée uniquement face à une requete HTTP de type get

    public ModelAndView chooseMeal(@RequestParam Map<String, Object> map) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("cart");

        // recuperation des items de chaque champs remplis
        // en parcourant la hashmap

        Iterator iter = map.entrySet().iterator();

        List<CommandLine> itemsOrdered = new ArrayList();

        while (iter.hasNext()) {
            Map.Entry mEntry = (Map.Entry) iter.next();

            // un produit est commandé, on traite
            if (!mEntry.getValue().toString().equals("") && Integer.parseInt(mEntry.getValue().toString()) > 0) {
                // on crée une ligne de commande par entrée
                CommandLine commandLine = new CommandLine(Integer.parseInt(mEntry.getValue().toString(),
                        Integer.parseInt(mEntry.getKey().toString())));
                itemsOrdered.add(commandLine);
                //  commandLine.setCommand(command);
                System.out.print(commandLine.toString());
                // on crée une ligne de commande
                // on envoie la liste de lignes de commande en base
                // pour constituer une commande
                cls.addCommandLine(commandLine);
            }

            // itemsOrdered.add(mEntry.getValue().toString());
            // System.out.println(mEntry.getKey() + " : " + mEntry.getValue());
        }

        mav.getModel().put("itemsOrdered", itemsOrdered);


        // mise a dispo dans la JSP
        // mav.getModel().put("idRestaurant", Integer.parseInt(choixRestaurant.toString()));

        return mav; // le return redirige vers la page index.jsp car un
        // viewresolver a été défini dans le fichier spring-servlet.xml
    }

/*
    @RequestMapping(value = {"AddEnqueteInt"}, method = RequestMethod.GET)
    // on définit la ou les URL prises en charge par la méthode accueil
    // cette méthode est invoquée uniquement face à une requete HTTP de type get
    public ModelAndView addEnqueteInt() {

        ModelAndView mav = new ModelAndView();
        mav.setViewName("addint");

        List<Fait> faits = fs.recupereFait();
        List<Departement> departements = ds.recupereDepartement();
        List<SiteInt> listeSites = ss.recupereSiteInt();

        mav.getModel().put("faits", faits);
        mav.getModel().put("listeSites", listeSites);
        mav.getModel().put("departements", departements);

        return mav;
    }

    @RequestMapping(value = {"addQuestion"}, method = RequestMethod.GET)
    // on définit la ou les URL prises en charge par la méthode accueil
    // cette méthode est invoquée uniquement face à une requete HTTP de type get
    public ModelAndView addQuestion(@RequestParam Map<String, Object> map) {

        ModelAndView mav = new ModelAndView();
        mav.setViewName("/addQuestion");

        mav.getModel().put("idEnquete", map.get("idEnquete"));

        return mav;
    }

    // le value correspond à la valeur de l'url qui utilisera ce Model and View
    // puis s'en suit la méthode
    @RequestMapping(value = {"addQuestion"}, method = RequestMethod.POST)
    public ModelAndView addQuestionPOST(@RequestParam Map<String, Object> map) {

        // on declare l'utilisation de l'objet model and view
        ModelAndView mav = new ModelAndView();

        //on declare le nom de la page
        mav.setViewName("/addQuestion");

        // on recupere l'ID de l'enquete pour cette question
        int idEnquete = Integer.parseInt(map.get("idEnquete").toString());

        String message;

        if(es.createQuestion(map.get("nom").toString(),
                Integer.parseInt(map.get("idEnquete").toString()))){
            message = "Question ajoutée avec succès!";
        } else {
            message = "Erreur insertion question!";
        }

        mav.getModel().put("message", message);

        return mav;
    }


    // le value correspond à la valeur de l'url qui utilisera ce Model and View
    // puis s'en suit la méthode
    @RequestMapping(value = {"AddEnqueteInt"}, method = RequestMethod.POST)
    public ModelAndView addEnqueteIntPOST(@RequestParam Map<String, Object> map) {

        // on declare l'utilisation de l'objet model and view
        ModelAndView mav = new ModelAndView();

        //on declare le nom de la page
        mav.setViewName("addint");

        // on recupere l'ID du site partenaire pour cette enquete
        int idSite = Integer.parseInt(map.get("site").toString());
        List<SiteInt> sites = new ArrayList<>();
        sites.add(ss.recupereSiteInt(idSite));

        // on recupere la date pour l'enregistrement de cette enquete
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String string = map.get("date").toString();
        Date date = null;
        try {
            date = sdf.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String message;
        //prix = Float.parseFloat(String.valueOf(map.get("prix")));

        // if(es.createEnquete(map.get("name").toString(), Float.parseFloat(map.get("prix").toString()), date, sites)){
        if(es.createEnquete(map.get("name").toString(), 15f, date, sites)){
            message = "Enquête insérée avec succès!";
        } else {
            message = "Erreur insertion!";
        }


        List<Fait> faits = fs.recupereFait();
        List<Departement> departements = ds.recupereDepartement();
        List<SiteInt> listeSites = ss.recupereSiteInt();

        mav.getModel().put("faits", faits);
        mav.getModel().put("sites", listeSites);
        mav.getModel().put("departements", departements);

        mav.getModel().put("message", message);

        return mav;
    }



    @RequestMapping(value = {"AddEnqueteTel"}, method = RequestMethod.GET)
    public ModelAndView addEnqueteTel() {

        ModelAndView mav = new ModelAndView();
        mav.setViewName("addtel");

        List<Fait> faits = fs.recupereFait();
        List<Departement> departements = ds.recupereDepartement();
        List<SiteInt> sites = ss.recupereSiteInt();

        mav.getModel().put("faits", faits);
        mav.getModel().put("sites", sites);
        mav.getModel().put("departements", departements);

        return mav;
    }

    */
}
