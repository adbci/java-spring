package fr.humanbooster.fx.food.controller;

import fr.humanbooster.fx.food.business.Category;
import fr.humanbooster.fx.food.business.CommandLine;
import fr.humanbooster.fx.food.business.MenuItem;
import fr.humanbooster.fx.food.business.Restaurant;
import fr.humanbooster.fx.food.service.CategoryService;
import fr.humanbooster.fx.food.service.CommandLineService;
import fr.humanbooster.fx.food.service.MenuItemService;
import fr.humanbooster.fx.food.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Human Booster on 06/03/2017.
 */
@Controller
// classe qui traite les requetes http
// en remplacement des servlets précédemments écrites
public class ManagerController {

    // injection d'une dépendance au service EnqueteService
    @Autowired // on declare un objet en précisant le nom d'une interface
    private RestaurantService rs;

    @Autowired
    private MenuItemService mis;

    @Autowired
    private CommandLineService cls;

    @Autowired
    private CategoryService cs;

    @RequestMapping(value = {"displayOrders"}, method = RequestMethod.GET)
    // on définit la ou les URL prises en charge par la méthode accueil
    // cette méthode est invoquée uniquement face à une requete HTTP de type get

    public ModelAndView displayOrders() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("orders");

        // recuperation des restaurants
        List<Restaurant> restaurants = rs.getAllRestaurants();

        // mise a dispo dans la JSP
        mav.getModel().put("restaurants", restaurants);

        return mav; // le return redirige vers la page index.jsp car un
        // viewresolver a été défini dans le fichier spring-servlet.xml
    }


    @RequestMapping(value = {"displayOrdersById"}, method = RequestMethod.GET)
    // on définit la ou les URL prises en charge par la méthode accueil
    // cette méthode est invoquée uniquement face à une requete HTTP de type get

    public ModelAndView displayOrdersById(@RequestParam Map<String, Object> map) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("displayOrdersById");

        // recuperer l'id de l'url ici

        // find orders by specific restaurant id ici

        return mav;
    }

}
